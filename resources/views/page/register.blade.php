<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
<h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>

        <form action="/kirim" method="post">
            @csrf
            <label for="fname">First name:</label><br>
            <br>
            <input type="text" name="fname"><br>
            <br>
            <label for="lname">Last name:</label><br>
            <br>
            <input type="text" name="lname"><br>
            
            <p>Gender:</p>
            <input type="radio" name="JK" value="cowo">
            <label for="cowo"> Male </label><br>
            <input type="radio" name="JK" value="cewe">
            <label for="cewe"> Female </label><br>
            <input type="radio" name="JK" value="other">
            <label for="other"> Other </label><br>

            <p>Nationality:</p>
            <select name="Negara">
            <option value="1">Indonesia</option>
            <option value="2">Amerika</option>
            <option value="3">Inggris</option>
            </select>

            <p>Language Spoken:</p>
            <input type="checkbox" id="bahasa1" name="bahasa1" value="Indonesia">
            <label for="bahasa1"> Bahasa Indonesia </label><br>
            <input type="checkbox" id="bahasa2" name="bahasa2" value="Inggris">
            <label for="bahasa2"> English </label><br>
            <input type="checkbox" id="bahasa3" name="bahasa3" value="Other">
            <label for="bahasa3"> Other </label><br>
            

            <p>Bio:</p>
            <textarea name="message" rows="10" cols="30"></textarea> <br>
            <br>

            <input type="submit" value="Sign Up">
             
        </form>
</body>
</html>