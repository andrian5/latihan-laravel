@extends('layout.master')

@section('judul')
Detail Cast {{$cast->nama}}
@endsection

@section('subjudul')
Informasi data secara lengkap
@endsection
    
@section('content')

<h1>{{$cast->nama}}</h1>
<h3>{{$cast->umur}}</h3>
<p>{{$cast->bio}}</p>

@endsection